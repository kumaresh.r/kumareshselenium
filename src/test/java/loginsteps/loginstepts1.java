package loginsteps;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class loginstepts1 {
	public ChromeDriver driver;
	
	@Given("Launch the Browser")
	public void launchTheBrowser() {
		System.setProperty("webdriver.chrome.driver","./Drivers/chromedriver.exe");
		driver = new ChromeDriver();
		
	}

	@And("Load the URL")
	public void loadTheURL() {
		driver.get("http://leaftaps.com/opentaps");
	}

	@And("max the Browser")
	public void maxTheBrowser() {
		driver.manage().window().maximize();
	}
	
	@Given("Set the Timeouts")
	public void setTheTimeouts() {
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	@And("Enter the name")
	public void enterTheName() {
		driver.findElementById("username").sendKeys("DemoSalesManagae");
	}

	@And("Enter the password")
	public void enterThePassword() {
		driver.findElementById("password").sendKeys("crmsfa");
	}
	
	@Then("Print as Verified")
	public void printAsVerified() {
	    System.out.println("Enter as verified");
}
}