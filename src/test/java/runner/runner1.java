package runner;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;

public class runner1 {
@CucumberOptions(features="src/test/java/feature/login.feature",
							glue="steps",
							dryRun=false,
							snippets=SnippetType.CAMELCASE,
							monochrome=true,
							plugin= "pretty")
public class TestRunner extends AbstractTestNGCucumberTest{
 }
}