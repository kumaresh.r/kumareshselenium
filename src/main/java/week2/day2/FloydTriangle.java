package week2.day2;

import java.util.Scanner;

public class FloydTriangle {

	public static void main(String[] args) {
		Scanner in = new Scanner (System.in);
		System.out.println("Enter the number of Rows");
		int noofRows=in.nextInt();
		int num=1;
		for (int i=1;i<=noofRows;i++)
		{
			for (int j=0;j<=i;j++)
			{
				System.out.println(" "+num);
				num++;
			}
			System.out.println();
		}

	}

}
