package week2.day2;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class frame0439 {

	public static void main(String[] args) {
	System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
	ChromeDriver driver = new ChromeDriver();
	driver.get("http://www.leafground.com/pages/frame.html");
	driver.manage().window().maximize();
	
	// First Frame
	driver.switchTo().frame(0);
	WebElement touch = driver.findElementById("Click");
	System.out.println("Text Before the Click Action :");
	System.out.println("  "+touch.getText());
	touch.click();
	System.out.println("Text After the Click Action :");
	System.out.println("  "+touch.getText());
	driver.switchTo().defaultContent();
	
	// Second Frame
	driver.switchTo().frame(1);
	driver.switchTo().frame("frame2");
	WebElement enter = driver.findElementById("Click1");
	System.out.println("   ");
	System.out.println("Text Before the Click Action :");
	System.out.println("  "+enter.getText());
	enter.click();
	System.out.println("Text After the Click Action :");
	System.out.println("  "+enter.getText());
	driver.switchTo().defaultContent();
	
	// Third Frame
	driver.switchTo().frame(2);
	driver.switchTo().frame("frame2");
	WebElement msg = driver.findElementByTagName("body");
	System.out.println("   ");
	System.out.println("  "+msg.getText());
	System.out.println("3 Main Frames and 2 Nested Frames");
	driver.switchTo().defaultContent();
	driver.close();
	
	}

}
