package week2.day4;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class actionclass {



	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.flipkart.com/");
		driver.manage().window().maximize();
		driver.getKeyboard().sendKeys(Keys.ESCAPE);
		WebElement Electronics = driver.findElementByXPath("//span[text()='Electronics']");
		WebElement Asus = driver.findElementByXPath("//a[text()='Asus']");
		
		Actions builder = new Actions(driver);
		builder.moveToElement(Electronics).perform();
		
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.elementToBeClickable(Asus)).click();
		
		wait.until(ExpectedConditions.elementToBeClickable(Electronics)).click();
		
		
	}
	}

