package leaftap;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class projectMethod {
	public ChromeDriver driver;
	@BeforeMethod
	public void startApp() {

		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
		//browser
		driver = new ChromeDriver();
		//url
		driver.get("http://leaftaps.com/opentaps");
		//maximize
		driver.manage().window().maximize();
		//username
		WebElement UName = driver.findElementById("username");
		UName.sendKeys("Democsr");
		//password
		driver.findElementById("password").sendKeys("crmsfa");
		//Login
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByClassName("crmsfa").click();
		driver.findElementByLinkText("Create Lead").click();
	}

	@AfterMethod
	public void CloseApp() {
		driver.close();	
	}


}
