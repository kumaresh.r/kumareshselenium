package week4.day1;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class LearnExcel {

	
		public static void main(String[] args) throws IOException {
			// locate the workbook
			XSSFWorkbook workbook = new XSSFWorkbook("./Data/GetExcel.xlsx");
			// get the sheet
			XSSFSheet sheet = workbook.getSheetAt(0);
			// get no.of rows available in the sheet
			int rowcount = sheet.getLastRowNum();
			System.out.println("Row Count " + rowcount);
			// get no.of columns available in the sheet
			int colCount = sheet.getRow(0).getLastCellNum();
			System.out.println("Column Count " + colCount);
			// loop through each rows
			for (int i = 1; i <= rowcount; i++) {
				XSSFRow row = sheet.getRow(i);
				// loop through each columns
				for (int j = 0; j < colCount; j++) {
					// print the cell value
					String data = row.getCell(j).getStringCellValue();
					System.out.println(data);
				}
			}
			workbook.close();
		}
	}
	/*
	 * CellType cellType = row.getCell(j).getCellType();
	 * if(cellType==CellType.NUMERIC) { double numericCellValue =
	 * row.getCell(j).getNumericCellValue(); }
	 */
	//XSSFSheet sheet = workbook.getSheet("Sheet1");
	//System.out.println(sheet.getPhysicalNumberOfRows());
