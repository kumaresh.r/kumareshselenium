package week4.day2;

import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class LearnExceptionHandling {

		public static void main(String[] args) throws IOException, NullPointerException {

			System.out.println("Enter a value");
			Scanner sc = new Scanner(System.in);
			try {
				int num = sc.nextInt();
//				throw new RuntimeException();
				System.out.println(num / 0);
			} catch (ArithmeticException e) {
				System.err.println("Cannot be divisible by 0");

			} catch (InputMismatchException exp) {
				System.err.println("String is a invalid input");
			} catch (Exception exp) {

				System.err.println("Any kind of exception" + exp.getMessage());
			} finally {
				System.out.println("I am from finally block");
			}
		}
	}
