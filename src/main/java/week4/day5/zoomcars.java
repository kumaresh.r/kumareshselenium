package week4.day5;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class zoomcars {

	public static void main(String[] args) {
		
		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
		ChromeDriver driver= new ChromeDriver();
		driver.get("https://www.zoomcar.com/chennai/");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.findElementByXPath("//a[@title='Start your wonderful journey']").click();
		driver.findElementByXPath("//div[text()[normalize-space()='Thuraipakkam']]").click();
		driver.findElementByXPath("//button[text()='Next']").click();
		// Get the current date
		Date date = new Date();
		//Get only the date (and not month, year, time etc)
		DateFormat sdf = new SimpleDateFormat("dd");
		// Get today's date
		String today = sdf.format(date);
		// Convert to integer and add 1 to it
		int tomorrow = Integer.parseInt(today)+1;
		// Print tomorrow's date
		System.out.println(tomorrow);
		//driver.findElementByXPath("//div[text()[normalize-space()="+tomorrow+" ]]").click(); (or)
		driver.findElementByXPath("//div[contains(text(),27)]").click();
		driver.findElementByXPath("//button[text()='Next']").click();
		driver.findElementByXPath("//button[text()='Done']").click();
		List<WebElement> allPrices = driver.findElementsByXPath("//div[@class='price']");
		int size = allPrices.size();
		System.out.println("Total no of Cars :"+allPrices.size());
		List<String> price = new ArrayList<>();
		for (WebElement eachElement : allPrices) {
			String price = eachElement.getText();
			System.out.println(price);
		}
		
		Collections.sort(allPrices);
		String max = Collections.max(price);
		String actualmax = max.replaceAll("\\D", "" );
		System.out.println(max);
	}
}
