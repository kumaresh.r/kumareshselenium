package week5.day1;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class webtable {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://erail.in/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		WebElement src = driver.findElement(By.id("txtStationFrom"));
		src.clear();
		src.sendKeys("MAS", Keys.TAB);
		WebElement to = driver.findElement(By.id("txtStationTo"));
		to.clear();
		to.sendKeys("PDY", Keys.TAB);
		// Finding the Table
		WebElement table = driver.findElementByXPath("//table[@class='DataTable TrainList']");
		// Finding the Rows
		List<WebElement> rows = table.findElements(By.tagName("tr"));
		for (WebElement row : rows) {
			List<WebElement> tds = row.findElements(By.tagName("td"));
			System.out.println(tds.get(0).getText());
			
		}
	}
}
		
		
		
