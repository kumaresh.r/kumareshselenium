package homwwork120419;

import java.util.Scanner;

public class hw150419 {

	public static void main(String[] args) {

	//	1.Name Display as String
		Scanner s= new Scanner(System.in);
		System.out.println("Enter yout Name: ");
		String in=s.nextLine();
		char[] ch = in.toCharArray();
		System.out.println();
		System.out.println("Output: ");
		for (int i=0; i<ch.length; i++) {
			System.out.println(ch[i]);
		}
			
	
	//	2.Capitalize First Letter of each word 
		String caps="capitalize first letter of each word";
		System.out.println();
		System.out.println("Small Letters to Caps : "+caps);
		System.out.println();
		String[] n=caps.split(" ");
		for (String each:n) {
			char[]ch1=each.toCharArray();
			System.out.print(Character.toUpperCase(ch1[0]));
			for(int i=1; i<ch1.length; i++)
				System.out.print(ch1[i]);
			System.out.print(" ");
		}
		
	//	3.Hello World word play
		String word = "Hello World";
		System.out.println("Entered String - "+word);
		String[] split = word.split(" ");
		char[] charArray = split[1].toCharArray();
		String red = "";
		for(int i=charArray.length-1; i>=0; i--)
			red+=charArray[i];
		System.out.println();
		System.out.println("Output - "+split[0]+" "+red );
	}
}
