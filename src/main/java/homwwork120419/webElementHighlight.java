package homwwork120419;

import java.io.File;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class webElementHighlight {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://jqueryui.com/selectable/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(80, TimeUnit.SECONDS);
		driver.switchTo().frame(0);
		
		WebElement itemone = driver.findElementByXPath("//li[text()='Item 1']");
		WebElement itemtwo = driver.findElementByXPath("//li[text()='Item 3']");
		
		Actions act = new Actions(driver);
		driver.manage().timeouts().implicitlyWait(80, TimeUnit.SECONDS);
		act.sendKeys(itemone, Keys.CONTROL).sendKeys(itemtwo, Keys.CONTROL).perform();
		
	}

}
