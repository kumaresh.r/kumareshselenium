package homwwork120419;

import java.util.ArrayList;
import java.util.List;

public class descendingorder {

	public static void main(String[] args) {
		List<String> mobileList = new ArrayList<String>();
		
		mobileList.add("Honor");
		mobileList.add("Samsung");
		mobileList.add("Htc");
		mobileList.add("Moto");
		mobileList.add("Nokia");
		mobileList.add("Oneplus");
		mobileList.add("Asus");
		mobileList.add("Alienware");
		
		// Printing all the List Elements
		System.out.println("Size of Mobile Phone Brand List :" +mobileList.size());
		System.out.println("Mobile Phone Brands :");
		System.out.println();
		System.out.println(mobileList);
		
		// Removing First and Second Element
		mobileList.remove(0);
		mobileList.remove(1);
		
		// Printing Remaining List in Descending Order
		System.out.println();
		System.out.println("New Mobile Brand List :"+mobileList.size());
		System.out.println("Descending Order :");
		System.out.println("  ");
		for (int i=mobileList.size()-1;i>=0;i--) {
			System.out.println(mobileList.get(i));
		}
		System.out.println();
		
		
		
		
		
		

	}

}
