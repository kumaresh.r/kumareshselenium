package week1.day2;

public class CalculatorMain {

	public static void main(String[] args) {
    double a=30, b=20, c;
    
    // Calling all arithmetic methods from CalcMethod Class
    
    c=CalcMethods.add(a, b);
    System.out.println("Sum is : "+c);
    
    c=CalcMethods.sub(a, b);
    System.out.println("Subtration is : "+c);

    c=CalcMethods.mul(a, b);
    System.out.println("Multiplication is : "+c);
    
    c=CalcMethods.div(a, b);
    System.out.println("Division is : "+c);
    
	}

}
